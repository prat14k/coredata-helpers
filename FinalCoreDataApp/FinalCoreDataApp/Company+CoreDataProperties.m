//
//  Company+CoreDataProperties.m
//  FinalCoreDataApp
//
//  Created by Prateek Sharma on 7/10/17.
//  Copyright © 2017 Bingo Tech. All rights reserved.
//

#import "Company+CoreDataProperties.h"

@implementation Company (CoreDataProperties)

+ (NSFetchRequest<Company *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Company"];
}

@dynamic name;
@dynamic imageURL;
@dynamic mobiles;

@end
