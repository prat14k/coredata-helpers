//
//  CompanyCDTVController.m
//  FinalCoreDataApp
//
//  Created by Prateek Sharma on 7/10/17.
//  Copyright © 2017 Bingo Tech. All rights reserved.
//

#import "CompanyCDTVController.h"
#import "Company+CoreDataClass.h"
#import "MobileDBAvailabilityHeader.h"

@interface CompanyCDTVController ()

@end

@implementation CompanyCDTVController



- (void)awakeFromNib{
    
    [super awakeFromNib];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:MobileDBAvailabilityNotification object:nil queue:nil usingBlock:^(NSNotification *note){
        self.managedObjectContext = note.userInfo[MobileDBContext];
    }];
    
}



- (void)setManagedObjectContext:(NSManagedObjectContext *)managedObjectContext{
    
    _managedObjectContext = managedObjectContext;
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Company"];
    request.predicate = nil;//Fetch all
    request.sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(localizedStandardCompare:)]];
    
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = nil;
    
    cell = [self.tableView dequeueReusableCellWithIdentifier:@"tableCell"];
    
    Company *company = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    cell.textLabel.text = company.name;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%ld mobiles",[company.mobiles count]];
    
    return cell;
}


@end
