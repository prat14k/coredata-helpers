//
//  Company+CoreDataProperties.h
//  FinalCoreDataApp
//
//  Created by Prateek Sharma on 7/10/17.
//  Copyright © 2017 Bingo Tech. All rights reserved.
//

#import "Company+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Company (CoreDataProperties)

+ (NSFetchRequest<Company *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *name;
@property (nullable, nonatomic, copy) NSString *imageURL;
@property (nullable, nonatomic, retain) NSSet<Mobile *> *mobiles;

@end

@interface Company (CoreDataGeneratedAccessors)

- (void)addMobilesObject:(Mobile *)value;
- (void)removeMobilesObject:(Mobile *)value;
- (void)addMobiles:(NSSet<Mobile *> *)values;
- (void)removeMobiles:(NSSet<Mobile *> *)values;

@end

NS_ASSUME_NONNULL_END
