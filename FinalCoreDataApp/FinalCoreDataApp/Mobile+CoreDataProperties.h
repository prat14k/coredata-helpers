//
//  Mobile+CoreDataProperties.h
//  FinalCoreDataApp
//
//  Created by Prateek Sharma on 7/10/17.
//  Copyright © 2017 Bingo Tech. All rights reserved.
//

#import "Mobile+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Mobile (CoreDataProperties)

+ (NSFetchRequest<Mobile *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *name;
@property (nullable, nonatomic, copy) NSString *modelID;
@property (nullable, nonatomic, retain) Company *whichCompany;

@end

NS_ASSUME_NONNULL_END
