//
//  Mobile+CoreDataClass.h
//  FinalCoreDataApp
//
//  Created by Prateek Sharma on 7/10/17.
//  Copyright © 2017 Bingo Tech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Company;

NS_ASSUME_NONNULL_BEGIN

@interface Mobile : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Mobile+CoreDataProperties.h"
