//
//  Company+Many.m
//  FinalCoreDataApp
//
//  Created by Prateek Sharma on 7/10/17.
//  Copyright © 2017 Bingo Tech. All rights reserved.
//

#import "Company+Many.h"

@implementation Company (Many)

+ (Company *)companyWithName:(NSString *)name
                    imageUrl:(NSString *)url
        managedObjectContext:(NSManagedObjectContext *)managedObjectContext{
    
    Company *company = nil;
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Company"];
    request.fetchLimit = 220;
    request.predicate = [NSPredicate predicateWithFormat:@"name LIKE %@",name];
    
    NSError *error;
    
    NSArray *match = [managedObjectContext executeFetchRequest:request error:&error];
    
    if((!match) || (error) || ([match count] > 1)){
        NSLog(@"Error");
    }
    else if([match count] == 1){
        company = [match firstObject];
    }
    else{
        
        company = [NSEntityDescription insertNewObjectForEntityForName:@"Company" inManagedObjectContext:managedObjectContext];
        company.name = name;
        company.imageURL = url;
        
    }
    
    return company;
}

@end
