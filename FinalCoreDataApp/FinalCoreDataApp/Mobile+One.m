//
//  Mobile+One.m
//  FinalCoreDataApp
//
//  Created by Prateek Sharma on 7/10/17.
//  Copyright © 2017 Bingo Tech. All rights reserved.
//

#import "Mobile+One.h"
#import "Company+Many.h"

@implementation Mobile (One)



+ (Mobile *)mobileWithInfo:(NSDictionary *)dict inMAnagedObjectContext:(NSManagedObjectContext *)managedObjectContext{
    
    Mobile *mobile = nil;
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Mobile"];
    request.fetchLimit = 210;
    request.predicate = [NSPredicate predicateWithFormat:@"modelID LIKE %@",[dict valueForKey:@"modelID"]];
    
    NSError *error;
    
    NSArray *matches = [managedObjectContext executeFetchRequest:request error:&error];
    
    if((!matches) || (error) || ([matches count] > 1)){
     
        NSLog(@"Errors");
        
    }
    else if([matches count] == 1){
        
        mobile = [matches firstObject];
        
    }
    else{
        mobile = [NSEntityDescription insertNewObjectForEntityForName:@"Mobile" inManagedObjectContext:managedObjectContext];
        
        mobile.modelID = [dict valueForKey:@"modelID"];
        mobile.name = [dict valueForKey:@"name"];
        
        mobile.whichCompany = [Company companyWithName:[dict valueForKey:@"company"] imageUrl:[dict valueForKey:@"url"] managedObjectContext:managedObjectContext];
        
    }
    
    
    
    return mobile;
}

+ (void)loadMobilesWithFetchedData:(NSArray *)jsonData inManagedObjectContext:(NSManagedObjectContext *)managedObjectContext{
    
    for(NSDictionary *dict in jsonData){
        [Mobile mobileWithInfo:dict inMAnagedObjectContext:managedObjectContext];
    }
    
}



@end
