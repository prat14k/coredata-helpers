//
//  AppDelegate.m
//  FinalCoreDataApp
//
//  Created by Prateek Sharma on 7/10/17.
//  Copyright © 2017 Bingo Tech. All rights reserved.
//

#import "AppDelegate.h"
#import "Mobile+One.h"
#import "MobileDBAvailabilityHeader.h"

@interface AppDelegate () <NSURLSessionDownloadDelegate>

@property (copy,nonatomic) void (^downloadCompletionHandler)();
@property (strong, nonatomic) NSURLSession *downloadSession;
@property (strong, nonatomic) NSTimer *timer;

@end

#define FETCHTASK @"New Data Fetching"

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    
    [self startDataFetch];
    
    [NSTimer scheduledTimerWithTimeInterval:20*10 target:self selector:@selector(startTimerFetch:) userInfo:nil repeats:YES];
    
    return YES;
}

- (void)startTimerFetch:(NSTimer *)timer{
    [self startDataFetch];
}

- (void)startDataFetch{
    
    [self.downloadSession getTasksWithCompletionHandler:^(NSArray *dataTasks,NSArray *uploadTasks,NSArray *downloadTasks){
        if(![downloadTasks count]){
            NSURLSessionDownloadTask *myDownloadTask = [self.downloadSession downloadTaskWithURL:[NSURL URLWithString:@"http://cameraapi.esy.es/fetch_all_mobiles.php" ]];
            
            myDownloadTask.taskDescription = FETCHTASK;
            [myDownloadTask resume];
        }
        else{
            for(NSURLSessionDownloadTask *task in downloadTasks){
                [task resume];
            }
        }
    }];
    
}

//Setting and getting
- (NSURLSession *)downloadSession{ //Will help to fetch in the background
    
    if(!_downloadSession){
        _downloadSession = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:FETCHTASK] delegate:self delegateQueue:nil];
    }
    
    return _downloadSession;
    
}





#pragma mark - DownloadSession Delegate tasks

/* Sent when a download task that has completed a download.  The delegate should
 * copy or move the file at the given location to a new location as it will be
 * removed when the delegate message returns. URLSession:task:didCompleteWithError: will
 * still be called.
 */
- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask
didFinishDownloadingToURL:(NSURL *)location{
    
    NSLog(@"Finished Data Load");
    
    
    if([downloadTask.taskDescription isEqualToString:FETCHTASK]){
        
        NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
        if(managedObjectContext){
            NSArray *jsonDict = [self retreiveDataFromLocation:location];
            [managedObjectContext performBlock:^(void){ //Async perform of the block
                [Mobile loadMobilesWithFetchedData:jsonDict inManagedObjectContext:managedObjectContext];
                [managedObjectContext save:nil];
            }];
        }
        else{
            [self downloadTasksMightBeComplete];
        }
        
    }
    
    
}

- (NSArray *)retreiveDataFromLocation:(NSURL *)locationURL{
    
    NSArray *data = nil;
    
    NSData *jsonData = [NSData dataWithContentsOfURL:locationURL];
    data = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableLeaves error:nil];
    NSLog(@"%@",data[0]);
    return data;
}


- (void)downloadTasksMightBeComplete{
    
    if(self.downloadCompletionHandler){
        [self.downloadSession getTasksWithCompletionHandler:^(NSArray *dataTasks,NSArray *uploadTasks,NSArray *downloadTasks){
           
            if(![downloadTasks count]){
                void (^completionHandler)() = self.downloadCompletionHandler;
                self.downloadCompletionHandler = nil;
                if(completionHandler){
                    completionHandler();
                }
            }
            
        }];
    }
    
}


- (void)setManagedObjectContext:(NSManagedObjectContext *)managedObjectContext{
    
    _managedObjectContext = managedObjectContext;
    
    NSDictionary *userInfo = _managedObjectContext ? @{ MobileDBContext : _managedObjectContext } : nil;
    [[NSNotificationCenter defaultCenter] postNotificationName:MobileDBAvailabilityNotification object:self userInfo:userInfo];
    
    
    
}



/* Sent periodically to notify the delegate of download progress. */
- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask
      didWriteData:(int64_t)bytesWritten
 totalBytesWritten:(int64_t)totalBytesWritten
totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite{
    
    NSLog(@"Download task periodic status call");
    
}

/* Sent when a download has been resumed. If a download failed with an
 * error, the -userInfo dictionary of the error will contain an
 * NSURLSessionDownloadTaskResumeData key, whose value is the resume
 * data.
 */
- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask
 didResumeAtOffset:(int64_t)fileOffset
expectedTotalBytes:(int64_t)expectedTotalBytes{
    
    NSLog(@"Download task resume call");
    
}






- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}



#pragma mark - Background Fetch Code

- (void)application:(UIApplication *)application performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler{
    [self startDataFetch];
    completionHandler(UIBackgroundFetchResultNoData);
}

- (void)application:(UIApplication *)application handleEventsForBackgroundURLSession:(NSString *)identifier completionHandler:(void (^)())completionHandler{
    self.downloadCompletionHandler = completionHandler;
}



#pragma mark - Core Data stack
@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
@synthesize persistentContainer = _persistentContainer;


// Returns the URL to the application's Documents directory

- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (void)saveContext:(NSManagedObjectContext *)managedObjectContext
{
    NSError *error = nil;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *managedObjectContext = nil;
    NSPersistentStoreCoordinator *coordinator = self.persistentStoreCoordinator;
    if (coordinator != nil) {
        managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        [managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    _managedObjectContext = managedObjectContext;
    
    
    NSDictionary *userInfo = _managedObjectContext ? @{ MobileDBContext : _managedObjectContext } : nil;
    [[NSNotificationCenter defaultCenter] postNotificationName:MobileDBAvailabilityNotification object:self userInfo:userInfo];
    
    return managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    NSManagedObjectModel *managedObjectModel = nil;
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"CoreDataModel" withExtension:@"momd"];
    managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    _managedObjectModel = managedObjectModel;
    return managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    NSPersistentStoreCoordinator *persistentStoreCoordinator = nil;
    NSManagedObjectModel *managedObjectModel = self.managedObjectModel;
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"MOC.sqlite"];
    
    NSError *error = nil;
    persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:managedObjectModel];
    if (![persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    _persistentStoreCoordinator = persistentStoreCoordinator;
    return persistentStoreCoordinator;
}



- (NSPersistentContainer *)persistentContainer {
    // The persistent container for the application. This implementation creates and returns a container, having loaded the store for the application to it.
    @synchronized (self) {
        if (_persistentContainer == nil) {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"FinalCoreDataApp"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                if (error != nil) {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    
                    /*
                     Typical reasons for an error here include:
                     * The parent directory does not exist, cannot be created, or disallows writing.
                     * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                     * The device is out of space.
                     * The store could not be migrated to the current model version.
                     Check the error message to determine what the actual problem was.
                     */
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                    abort();
                }
            }];
        }
    }
    
    return _persistentContainer;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}




@end
