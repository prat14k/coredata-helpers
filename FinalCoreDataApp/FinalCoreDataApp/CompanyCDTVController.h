//
//  CompanyCDTVController.h
//  FinalCoreDataApp
//
//  Created by Prateek Sharma on 7/10/17.
//  Copyright © 2017 Bingo Tech. All rights reserved.
//

#import "CoreDataTableViewController.h"

@interface CompanyCDTVController : CoreDataTableViewController

@property (strong,nonatomic) NSManagedObjectContext *managedObjectContext;

@end
