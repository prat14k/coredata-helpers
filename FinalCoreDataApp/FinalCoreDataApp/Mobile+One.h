//
//  Mobile+One.h
//  FinalCoreDataApp
//
//  Created by Prateek Sharma on 7/10/17.
//  Copyright © 2017 Bingo Tech. All rights reserved.
//

#import "Mobile+CoreDataClass.h"

@interface Mobile (One)

+ (Mobile *)mobileWithInfo:(NSDictionary *)dict
    inMAnagedObjectContext:(NSManagedObjectContext *)managedObjectContext;


+ (void)loadMobilesWithFetchedData:(NSArray *)jsonData
            inManagedObjectContext:(NSManagedObjectContext *)managedObjectContext;

@end
