//
//  Mobile+CoreDataProperties.m
//  FinalCoreDataApp
//
//  Created by Prateek Sharma on 7/10/17.
//  Copyright © 2017 Bingo Tech. All rights reserved.
//

#import "Mobile+CoreDataProperties.h"

@implementation Mobile (CoreDataProperties)

+ (NSFetchRequest<Mobile *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Mobile"];
}

@dynamic name;
@dynamic modelID;
@dynamic whichCompany;

@end
