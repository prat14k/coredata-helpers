//
//  Company+Many.h
//  FinalCoreDataApp
//
//  Created by Prateek Sharma on 7/10/17.
//  Copyright © 2017 Bingo Tech. All rights reserved.
//

#import "Company+CoreDataClass.h"

@interface Company (Many)


+ (Company *)companyWithName:(NSString *)name
                    imageUrl:(NSString *)url
        managedObjectContext:(NSManagedObjectContext *)managedObjectContext;

@end
