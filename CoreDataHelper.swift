//
//  CoreDataHelper.swift
//  Watchkit App
//
//  Created by Prateek Sharma on 28/12/17.
//  Copyright © 2017 Prateek Sharma. All rights reserved.
//

import Foundation
import CoreData

class CoreDataHelper : NSObject {
    
    class func sharedGroupURL() -> String {
        return "group.14k.WatchkitDemo"
    }
    
    class func managedObjectModel() -> NSManagedObjectModel {
        let proxyBundle = Bundle(identifier: "prat14k.WatchkitDemoProxyBundle")
        
        let modelURL = proxyBundle?.url(forResource: "LocationDataModel", withExtension: "momd")
        
        return NSManagedObjectModel(contentsOf: modelURL!)!
    }

    class func persistentStoreCoordinator() -> NSPersistentStoreCoordinator? {
        
        let sharedContainerURL : URL? = FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: CoreDataHelper.sharedGroupURL())
        
        if let sharedContainerURL = sharedContainerURL {
            let storeURL = sharedContainerURL.appendingPathComponent("db.sqlite")
            let coordinator : NSPersistentStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel: CoreDataHelper.managedObjectModel())
            
            do{
               try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: storeURL, options: nil)
                
                return coordinator
            }
            catch _{
                print("Persistent Store Coordinator Error")
            }
        }
        
        return nil
    }
    
    class func managedObjectContext() -> NSManagedObjectContext {
        let coordinator = CoreDataHelper.persistentStoreCoordinator()
        
        let managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        
        return managedObjectContext
    }
    
    
    class func insertManagedObject(className : String, managedObjectContext : NSManagedObjectContext) -> Any {
        let managedObject = NSEntityDescription.insertNewObject(forEntityName: className, into: managedObjectContext)
        
        return managedObject
    }
    
    class func saveManagedObjectContext(managedObjectContext : NSManagedObjectContext) -> Bool{
        do {
            try managedObjectContext.save()
            return true
        }
        catch _ {
            print("MOM Save Error")
        }
        
        return false
    }
    
    class func fetchEntities(className : String, predicate : NSPredicate? , sortDescriptor : NSSortDescriptor? , managedObjectContext : NSManagedObjectContext) -> [Any] {
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        
        fetchRequest.entity = NSEntityDescription.entity(forEntityName: className, in: managedObjectContext)
        
        if predicate != nil {
            fetchRequest.predicate = predicate
        }
        if sortDescriptor != nil {
            fetchRequest.sortDescriptors = [sortDescriptor!]
        }
        
        fetchRequest.returnsObjectsAsFaults = false
        
        do{
            let items = try managedObjectContext.fetch(fetchRequest)
            return items
        }
        catch _ {
            print("MOM fetch Error")
        }
        
        return []
    }
    
}

